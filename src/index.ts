import * as fs from 'fs';
import * as pathUtils from 'path';
import { process } from '@minidoc/processor/lib';
import { Document, Bundle, Group, Page, IParent, INode } from '@minidoc/core/lib';

const extensions = ['.mdoc', '.minidoc'];
const regex = new RegExp(/.*(?=\.)/);
const groupDocumentName = '.group';

function readFileContents(path: string): string {
    return fs.readFileSync(path).toString();
}

export function getBundle(path: string): Bundle {
    const bundle = new Bundle();
    const nodes = getNodes(path, bundle);

    bundle.children.push(...nodes);

    return bundle;
}

function getNodes(path: string, parent: IParent): INode[] {
    const nodes: INode[] = [];

    for (const obj of fs.readdirSync(path, { withFileTypes: true })) {
        const objPath = pathUtils.join(path, obj.name);

        if (obj.isDirectory()) {

            const possibleGroupDocs = extensions.map(extension =>
                pathUtils.join(objPath, `${groupDocumentName}${extension}`));

            let content: string | null = null;
            for (const groupDocPath of possibleGroupDocs) {
                if (fs.existsSync(groupDocPath)) {
                    content = readFileContents(groupDocPath);
                    break;
                }
            }

            let document: Document | null = null;
            if (content !== null) {
                document = process(content);
            }

            const group = new Group(obj.name, parent, document);
            group.children.push(...getNodes(objPath, group));

            nodes.push(group);

        } else if (obj.isFile() &&
            !obj.name.startsWith('.') &&
            extensions.some(extension => obj.name.endsWith(extension))) {

            // @ts-expect-error the if statement above ensures regex match
            const id = regex.exec(obj.name).toString();
            const content = readFileContents(objPath);

            let document: Document | null = null;
            if (content !== null) {
                document = process(content);
            }

            const page = new Page(id, parent, document);
            nodes.push(page);
        }
    }

    return nodes;
}
